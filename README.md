# Database consideration

The Database used for this project, is an PostgresSQL 9.6 database, so you can
restore the Database that I supplied, and to get everything working like a 
charm you have to edit the file ./webtask_server/facade/vehicle_facade.js.

In the second line you would see the database credentials, so change them using
your own DB credentials.

# How to run the Rest API Server

The Rest API services was developed using NodeJS, to run it, please first
install all the needed libraries, executing inside webtask_rest directory the
next command:

### `npm install`

get inside 
webtask_rest directory with using command line and run:

### `node index.js`

This API will retrieve all the needed data to the ReactJS project.


# How to run the ReactJS project

To run the ReactJS project, please get inside the webtask directory install all
the libraries executing:

### `npm install`

Then using the command line you can run:

### `npm start`

it runs the app in the development mode.

You can open [http://localhost:3000] to view it in the browser.

If you want to see the project using a mobile, you first have to get the IP 
of your computer and then open the file .webtask/src/enviroment.js and change
all the lines that says [http://localhost:8000] to the corresponding IP of your
computer, for example [http://192.168.0.100:8000].

Then write that IP on your mobile browser, for example 
[http://192.168.0.100:3000].

Or the simplest way to see the content like using a mobile is using the Browser
Developer Console to emulate a mobile screen
