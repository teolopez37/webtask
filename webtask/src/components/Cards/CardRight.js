import React, { useContext } from 'react'
import CardLine from './CardLine'
import { DataContext } from '../../App'

export default function CardRight() {
    const { data } = useContext(DataContext)

    return (
        <div className='card card-right'>
            <p className='custom-card-title heading-four'>PERFORMANCE</p>
            {
                data.currentVehicleSpecs.map((item, index) => {
                    if (item.type === 'interior')
                        return <CardLine
                            key={item.label}
                            label={item.label}
                            value={item.value}/>
                    else
                        return false
                })
            }
        </div>
    )
}
