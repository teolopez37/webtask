import React, { useContext } from 'react'

import CardLine from './CardLine'
import { DataContext } from '../../App'

export default function CardLeft() {
    const { data } = useContext(DataContext)

    return (
        <div className='card card-left'>
            <p className='custom-card-title heading-four'>EXTERIOR</p>
            {
                data.currentVehicleSpecs.map((item, index) => {
                    if (item.type === 'exterior')
                        return <CardLine
                            key={item.label}
                            label={item.label}
                            value={item.value} />
                    else
                        return false
                })
            }
        </div>
    )
}
