const initialData = {
    currentVehicle: {},
    currentVehicleSpecs: [],
    currentVehicleUrls: [],
    selectedIndexImage: 0
}


const dataReducer = (data, action) => {
    switch (action.type) {
        case 'SET_CURRENT_VEHICLE':
            return {
                ...data,
                currentVehicle: action.payload.vehicle
            };
        case 'SET_CURRENT_VEHICLE_SPECS':
            return {
                ...data,
                currentVehicleSpecs: action.payload.vehicleSpecs
            };
        case 'SET_CURRENT_VEHICLE_URLS':
            return {
                ...data,
                currentVehicleUrls: action.payload.vehicleUrls
            };
        case 'SET_SELECTED_INDEX_IMAGE':
            return {
                ...data,
                selectedIndexImage: action.payload.index
            };
        default:
            return data;
    }
}

export default [initialData, dataReducer];