import React from 'react'

export default function MainCounterItem(props) {
    const { label, value } = props
    return (
        <div className='counter-item'>
            <div className='counter-label'>{label}</div>
            <div className='counter-value'>{value}</div>
        </div>
    )
}
