import React from 'react'

export default function MainSpec(props) {
    const { label, value } = props
    return (
        <>
            <p className='heading-nine spec-label'>{label}</p>
            <p className='heading-four spec-value'>{value}</p>
        </>
    )
}
