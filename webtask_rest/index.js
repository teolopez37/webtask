const express = require('express')
const app = express()
const port = 8000
const bodyParser = require('body-parser')
const cors = require('cors');

const vehicle_logic = require('./logic/vehicle_logic')

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

app.get('/', (request, response) => {
  response.json({ info: 'Webtask Server Running' })
})

app.get('/vehicles/get_all', function (request, response) {
  vehicle_logic.getAll(request, response);
})

app.get('/vehicles/get_specs', function (request, response) {
  vehicle_logic.getSpecs(request, response);
})

app.get('/vehicles/get_image_urls', function (request, response) {
  vehicle_logic.getImageUrls(request, response);
})