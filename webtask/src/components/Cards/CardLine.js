import React from 'react'

export default function CardLine(props) {
    const { label, value } = props
    return (
        <div className={label === 'Engine' ? 'card-line' : 'card-line card-line-border'}>
            <p className='card-label heading-seven'>{label}</p>
            <p className='card-value heading-eight'>{value}</p>
        </div>
    )
}
