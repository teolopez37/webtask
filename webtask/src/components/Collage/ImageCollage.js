import './Collage.css'

import React, { useContext } from 'react'

import { DataContext } from '../../App'

export default function ImageCollage() {
    const { data, dataDispatch } = useContext(DataContext)

    return (
        <div className='image-collage'>
            {
                data.currentVehicleUrls.map((item, index) => {
                    return <img 
                        key={index} 
                        onClick={() => dataDispatch({ type: 'SET_SELECTED_INDEX_IMAGE', payload: { index: index } })}
                        src={item.url} 
                        alt={'collage-item'} />
                })
            }
        </div>
    )
}
