import React, {useContext} from 'react'

import Carousel from 'react-bootstrap/Carousel'

import { DataContext } from '../../App'

export default function MainCarrousel() {
    const { data } = useContext(DataContext)
    return (
        <Carousel>
            {data.currentVehicleUrls.map((item, index) =>{
                return <Carousel.Item key={index.toString()}>
                    <img
                        className="d-block w-100"
                        src={item.url}
                        alt={`Carousel slide ${index}`}
                    />
                </Carousel.Item>

            })}
        </Carousel>
    )
}
