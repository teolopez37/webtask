const Pool = require('pg').Pool;
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'webtask',
    password: '0992904788',
    port: 5432,
});

const getAll = async () => {
    try {
        let query = `
                SELECT  v.id, vm."name" as model, vb."name" as brand, v."year", v.lower_price, v.higher_price, v.current_use, u."name" as uom, v."number", v.vin, v."views", v.saves, v.shares
                FROM vehicle v
                JOIN vehicle_model vm
                  ON v.vehicle_model_id = vm.id
                JOIN vehicle_brand vb
                ON vm.vehicle_brand_id = vb.id
                JOIN vehicle_spec vs
                ON vs.vehicle_id = v.id
                JOIN vehicle_spec_type vst
                ON vs.vehicle_spec_type_id = vst.id
                JOIN vehicle_image vi
                ON vi.vehicle_id = v.id
                JOIN uom u
                ON v.uom_id = u.id
                GROUP BY  v.id, vm."name", vb.id, v."year", v.lower_price, v.higher_price, v.current_use, u."name", v."number", v.vin, v."views", v.saves; `;
        const result = await pool.query(query);
        if (!result || !result.rows || !result.rows.length) return null;
        return result.rows;
    } catch (error) {
        console.warn(error);
    }
}

const getSpecs = async (id) => {
    try {
        let query = `
            SELECT vs."label" , vs.value, vst."name" as type
                FROM vehicle_spec vs
                JOIN vehicle_spec_type vst
                ON vs.vehicle_spec_type_id = vst.id
                JOIN vehicle v ON v.id = vs.vehicle_id AND v.id = $1
                order by vs.id, vs."label" ;
        `;
        const result = await pool.query(query, [id]);
        if (!result || !result.rows || !result.rows.length) return null;
        return result.rows;
    } catch (error) {
        console.erro(error);
    }
}

const getImageUrls = async (id) => {
    try {
        let query = `
            SELECT vi.url
                FROM vehicle_image vi
                JOIN vehicle v ON v.id = vi.vehicle_id AND v.id = $1
                ORDER BY vi.id;
        `;
        const result = await pool.query(query, [id]);
        if (!result || !result.rows || !result.rows.length) return null;
        return result.rows;
    } catch (error) {
        console.warn(error);
    }
}

module.exports = {
    getAll,
    getSpecs,
    getImageUrls
}