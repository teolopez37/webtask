import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { isMobile } from 'react-device-detect'
import React, { useEffect, useReducer } from 'react'

import Header from './components/Header/Header'
import MainGallery from './components/Main/MainGallery'
import ImageCollage from './components/Collage/ImageCollage'
import CardsInfo from './components/Cards/CardsInfo'
import RequestActions from './actions/RequestActions'
import dataArray from './reducers/data'

export const DataContext = React.createContext()

function App() {

  const [initialData, dataReducer] = dataArray;
  const [data, dataDispatch] = useReducer(dataReducer, initialData);

  useEffect(() => {
    initializeData()
  }, [])

  const initializeData = async() =>{
    RequestActions.getAll().then((response) => {
      dataDispatch({ type: 'SET_CURRENT_VEHICLE', payload: { vehicle: response.data[0] } });
      initializeSpecs(response.data[0].id)
      initializeImageUrls(response.data[0].id)
    }).catch((error) => {
      console.error(error)
    });
  }

  const initializeSpecs = async(id) =>{
    RequestActions.getSpecs(id).then((response) => {
      dataDispatch({ type: 'SET_CURRENT_VEHICLE_SPECS', payload: { vehicleSpecs: response.data } });
    }).catch((error) => {
      console.error(error)
    });
  }

  const initializeImageUrls = async(id) =>{
    RequestActions.getImageUrls(id).then((response) => {
      dataDispatch({ type: 'SET_CURRENT_VEHICLE_URLS', payload: { vehicleUrls: response.data } });
    }).catch((error) => {
      console.error(error)
    });
  }

  return (
    <div className="App">
      <DataContext.Provider value={{ data: data, dataDispatch: dataDispatch }}>
        <Header />
        <MainGallery />
        {!isMobile && <ImageCollage />}
        {isMobile && <button className='btn btn-call heading-one'>CALL US</button>}
        <CardsInfo />
      </DataContext.Provider>
    </div>
  );
}

export default App
