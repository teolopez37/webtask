import './Header.css'
import React from 'react'
import { MdLocationOn, MdTimer, MdLocalPhone, MdSearch } from 'react-icons/md'

export default function Header() {
    return (
        <div className='header'>
            <div className='header-item rotate-left'>
                <a href='#' className='header-icon'><MdTimer /></a>
            </div>
            <div className='header-item rotate-left'>
                <a href='#' className='header-icon'><MdLocalPhone /></a>
            </div>
            <div className='header-item rotate-left'>
                <a href='#' className='header-icon'><MdLocationOn /></a>
            </div>
            <div className='header-black-item'>
                <a href='#' className='header-icon'><MdSearch /></a>
            </div>
        </div>
    )
}
