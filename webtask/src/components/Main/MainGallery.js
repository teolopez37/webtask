import React from 'react'
import MainPhoto from './MainPhoto'
import MainInfoPanel from './MainInfoPanel'
import MainCarrousel from './MainCarrousel'

import { isMobile } from 'react-device-detect'

export default function MainGallery() {

    return (
        <div className='main-gallery'>
            {isMobile ? <MainCarrousel/> : <MainPhoto /> }
            <MainInfoPanel />
        </div>
    )
}
