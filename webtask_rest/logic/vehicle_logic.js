const vehicle_facade = require('../facade/vehicle_facade')

const getAll = async (request, response) => {
	let vehicles = await vehicle_facade.getAll();
	response.status(200).json(
		{ code: 200, message: 'success', data: vehicles }
	);
}

const getSpecs = async (request, response) => {
	let specs = await vehicle_facade.getSpecs(request.query.vehicle_id);
	response.status(200).json(
		{ code: 200, message: 'success', data: specs }
	);
}

const getImageUrls = async (request, response) => {
	let urls = await vehicle_facade.getImageUrls(request.query.vehicle_id);
	response.status(200).json(
		{ code: 200, message: 'success', data: urls }
	);
}

module.exports = {
	getAll,
	getSpecs,
	getImageUrls
}