import './Cards.css'
import React from 'react'
import CardLeft from './CardLeft'
import CardRight from './CardRight'

export default function CardsInfo() {
    return (
        <div className='cards-info'>
            <CardLeft />
            <CardRight />
        </div>
    )
}
