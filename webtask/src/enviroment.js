var _Environments = {
    production:  {
        BASE_URL: 'http://localhost:8000',
    },
    staging:     {
        BASE_URL: 'http://localhost:8000',
    },
    development: {
        BASE_URL: 'http://localhost:8000',
    },
}

function getEnvironment() {
    var platform = 'production'

    return _Environments[platform]
}

var Environment = getEnvironment()
module.exports = Environment