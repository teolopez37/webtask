import './Main.css'

import React, { useContext } from 'react'
import { isMobile } from 'react-device-detect'
import { MdEmail } from 'react-icons/md'
import NumberFormat from 'react-number-format'

import MainSpec from './MainSpec'
import MainCounterItem from './MainCounterItem'
import { DataContext } from '../../App'


export default function MainInfoPanel() {
    const { data } = useContext(DataContext);
    const { model, brand, year, lower_price, higher_price, current_use, uom, number, vin, views, saves, shares } = data.currentVehicle

    return (
        <div className='main-info-panel'>
            <div className='main-info-pannel-container'>
                <div className='main-panel-one'>
                    <p className='heading-two title'>{`${brand && brand.toUpperCase()} ${model && model.toUpperCase()}`}</p>
                    <MainSpec label={'Year'} value={year} />
                    <MainSpec label={'Price range'} 
                        value={<>
                            <NumberFormat value={lower_price} displayType={'text'} thousandSeparator={true} prefix={'$'}/> <NumberFormat value={higher_price} displayType={'text'} thousandSeparator={true} prefix={'- $'}/>
                        </>
                        } />
                    <MainSpec label={'Usage'} value={<>
                        <NumberFormat value={current_use} displayType={'text'} thousandSeparator={true}/>
                        {` ${uom}`}
                    </>} />
                </div>
                <div className='main-panel-two'>
                    <p className='heading-six item-number'>{`Item Number: ${number}`}</p>
                    <p className='heading-six item-vin'>{`VIN: ${vin}`}</p>
                    <p className='item-share'>Share this car <MdEmail /></p>
                    <div className='counter-section'>
                        <MainCounterItem label={'Views'} value={views} />
                        {!isMobile && <MainCounterItem label={'Saves'} value={saves} />}
                        {!isMobile && <MainCounterItem label={'Shares'} value={shares} />}
                    </div>
                </div>
            </div>
        </div>
    )
}
