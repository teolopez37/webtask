import React, { useContext } from 'react'

import { DataContext } from '../../App'

export default function MainPhoto() {
    const { data } = useContext(DataContext)
    const { currentVehicleUrls, selectedIndexImage } = data

    return (
        <div className='main-photo'>
            {data.currentVehicleUrls.length > 1 && <img className='main-photo-img' src={currentVehicleUrls[selectedIndexImage].url} alt='main' />}
        </div>
    )
}
