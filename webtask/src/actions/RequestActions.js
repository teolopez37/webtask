import Enviroment from '../enviroment'
import { defer } from 'q'

let RequestActions = {
    getAll: function() {
        let deferred = defer();
        fetch(`${Enviroment.BASE_URL}/vehicles/get_all`, {
            crossDomain:true,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((res) => {
            if(res.code === 200){
                deferred.resolve(res);
            }
        })
        .catch((error) => {
            deferred.reject(`Error during getAll: ${String(error)}`);
        });
        return deferred.promise;
    },
    getSpecs: function(id) {
        let deferred = defer();
        fetch(`${Enviroment.BASE_URL}/vehicles/get_specs?vehicle_id=${id}`, {
            crossDomain:true,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((res) => {
            if(res.code === 200){
                deferred.resolve(res);
            }
        })
        .catch((error) => {
            deferred.reject(`Error during getSpecs: ${String(error)}`);
        });
        return deferred.promise;
    },
    getImageUrls: function(id) {
        let deferred = defer();
        fetch(`${Enviroment.BASE_URL}/vehicles/get_image_urls?vehicle_id=${id}`, {
            crossDomain:true,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((response) => response.json())
        .then((res) => {
            if(res.code === 200){
                deferred.resolve(res);
            }
        })
        .catch((error) => {
            deferred.reject(`Error during getImageUrls: ${String(error)}`);
        });
        return deferred.promise;
    },
}


export default RequestActions